<?php
   include 'php/consultaPyP.php';
   ?>
<!DOCTYPE HTML>
<html lang="en">
   <head>
      <title>Pico y Placa</title>
      <!-- Meta-Tags -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta charset="utf-8">
      <meta name="keywords" content="">
      <script>
         addEventListener("load", function () {
         	setTimeout(hideURLbar, 0);
         }, false);
         
         function hideURLbar() {
         	window.scrollTo(0, 1);
         }
      </script>
      <!-- //Meta-Tags -->
      <!-- Stylesheets -->
      <link href="css/styles.css" rel='stylesheet' type='text/css' />
      <!--// Stylesheets -->
      <!--fonts-->
      <!-- title -->
      <!-- body -->
      <link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=devanagari,latin-ext" rel="stylesheet">
      <!--//fonts-->
   </head>
   <body>
      <div class="fondopyp1">
         <?php 
            date_default_timezone_set('America/Bogota');		
            $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo");
            $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            $dia = $dias[date("w")];
            $dianum = date("j");
            $mes = $meses[date('n')-1];
         ?>
         <div class="cabezote">
            <h6>Pico y Placa Municipio de Sabaneta</h6>
         </div>
         <?php echo '<div style="height: 5%;"><h1 Style="height:100%;">' . $dia . ' ' . $dianum . ' de ' . $mes . '</h1></div>'?>
         <div class="containerpyp1">
            <?php 
               $result = consultaPicoPlaca ($dia);
               
               $cantidadVehiculos = $result->num_rows;
               $tamanoRecuadro = 100 / $cantidadVehiculos;
               while ($fila=mysqli_fetch_array($result)){
               	echo '<div Style="width:' . $tamanoRecuadro . '%;" class="fondoNegro-pyp1">
               	<img class="iconos" src="images\\' . $fila['alias'] . '.png">
               	<h3>' . $fila['nombreVehiculo'] . '</h3>';
               	if (strpos($fila['digitosRestriccion'], 'Sin Restriccion') !== false){	
               		echo '<h4 style="margin: 0% 5%; padding-top:5%;padding-bottom:5%;">Sin Restricción</h4>';
               	}else {	
               		if (strpos($fila['alias'], 'moto') !== false){
               			echo '<h5>Placa inicia en</h5>';
               		}else {
               			echo '<h5>Placa finaliza en</h5>';
               		}
               		echo '<h4>' . $fila['digitosRestriccion'] . '</h4>
               		<h5>' . $fila['horarioInicial1'] . ' a ' . $fila['horarioFinal1'] . '</h5>';
               		if ($fila['horarioInicial2'] != ""){
               			echo '<h5>' . $fila['horarioInicial2'] . ' a ' . $fila['horarioFinal2'] . '</h5>';	
               		}
               	}
               	echo '</div>';			
               }
            ?>
         </div>
         <div class="footer-pyp">
            <p>© 2019 Yoursoft Team. All Rights Reserved | Design by
               <a href="http://Yoursoft.com/" target="_blank">Yoursoft</a>
            </p>
         </div>
      </div>
   </body>
</html>
